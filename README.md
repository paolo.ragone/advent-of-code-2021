# How to run

Just clone the repo, run `yarn` in the dir so it download the necessary dependencies.

Then do:
`yarn ts-node src/<the day>/index.ts`

And it'll run the exercise for that day.
