
class Probe {
  x: number;
  y: number;
  constructor(public xVelocity: number, public yVelocity: number) {
    this.x = 0;
    this.y = 0;
  }
  step() {
    this.x += this.xVelocity;
    this.y += this.yVelocity;
    this.xVelocity = this.xVelocity - Math.sign(this.xVelocity);
    this.yVelocity--;
  }
  hit(area: Area) {
    return this.x >= area.xMin && this.x <= area.xMax && this.y >= area.yMin && this.y <= area.yMax;
  }
}

type Area = {
  xMin: number;
  xMax: number;
  yMin: number;
  yMax: number;
};

function canHit(xVel: number, yVel: number, area: Area): boolean {
  const probe = new Probe(xVel, yVel);
  let i = 0;
  while (i < 300 && probe.x < area.xMax && probe.y > area.yMin) {
    probe.step();
    if (probe.hit(area)) {
      return true;
    }
  }
  return false;
}

const testProbe = new Probe(7,2);
for (let i = 0; i < 7; i++) {
  console.log('Pos:', testProbe.x, testProbe.y);
  testProbe.step();
}

const input_area = {
  xMin: 209,
  xMax: 238,
  yMin: -86,
  yMax: -59
};

let counter = 0;
let maxY = -1;
for (let y = -100; y < 200; y++) {
  const row = [];
  for (let x = 0; x < 500; x++) {
    const ch = canHit(x,y, input_area);
    row.push(ch ? 'X' : '.');
    if (ch) counter++;
    if (ch && y > maxY) {
      maxY = y;
    }
  }
  console.log(y, row.join(''));
}
console.log('Part 1: What is the highest y position it reaches on this trajectory?', maxY*(maxY+1)/2);
console.log('Part 2: How many distinct initial velocity values cause the probe to be within the target area after any step?', counter);
// 85 is the max Y that still hits. Therefore the max height is 85 + 84 + 83 + ...  + 1 = 85 * 86 / 2 = 3655