import { loadInputFile_StrByLine } from "../utils";

const sample_input_str = loadInputFile_StrByLine(__dirname, 'sample_input.txt');
const sample_input = sample_input_str.map(l => [...l].map(n => parseInt(n, 10)));

function getLowPoints(map: number[][]): number[] {
  const resp: number[] = [];
  for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[y].length; x++) {
      const thisVal = map[y][x];
      const isLowPoint = (x == 0 || map[y][x-1] > thisVal) &&
        (x == (map[y].length -1) || map[y][x+1] > thisVal) &&
        (y == 0 || map[y-1][x] > thisVal) &&
        (y == (map.length -1) || map[y+1][x] > thisVal);
      if (isLowPoint) {
        resp.push(thisVal);
      }
    }
  }
  return resp;
}

const sample_lowPoints = getLowPoints(sample_input);
// console.log(sample_lowPoints);
const sample_answer = sample_lowPoints.reduce((a,v) => a+v+1, 0);
console.assert(sample_answer === 15, 'Sample answer is incorrect');

// Part 1
const input_str = loadInputFile_StrByLine(__dirname, 'input.txt');
const input = input_str.map(l => [...l].map(n => parseInt(n, 10)));

const input_lowPoints = getLowPoints(input);
const part1_answer = input_lowPoints.reduce((a,v) => a+v+1, 0);

console.log('Part 1: What is the sum of the risk levels of all low points on your heightmap?', part1_answer);

// Part 2

type Point = {x: number, y: number};

class Basin {
  private visited: number[][];
  constructor(public lowPoint: Point, private map: number[][]) {
    this.visited = map.map(x => x.map(y => -1));
    this.visited[this.lowPoint.y][this.lowPoint.x] = 1;
  }
  visit(x: number, y: number, prevValue: number) {
    if (x < 0) return;
    if (x > (this.map[0].length - 1)) return;
    if (y < 0) return;
    if (y > (this.map.length - 1)) return;
    if (this.visited[y][x] !== -1) return;
    const thisVal = this.map[y][x];
    if (thisVal === 9) {
      this.visited[y][x] = 0; // Not in the basin
    } else if (thisVal > prevValue) {
      this.visited[y][x] = 1; // In the basin
      this.visit(x-1, y, thisVal);
      this.visit(x+1, y, thisVal);
      this.visit(x, y-1, thisVal);
      this.visit(x, y+1, thisVal);
    }
  }
  grow() {
    this.visit(this.lowPoint.x - 1, this.lowPoint.y, this.map[this.lowPoint.y][this.lowPoint.x]);
    this.visit(this.lowPoint.x + 1, this.lowPoint.y, this.map[this.lowPoint.y][this.lowPoint.x]);
    this.visit(this.lowPoint.x, this.lowPoint.y - 1, this.map[this.lowPoint.y][this.lowPoint.x]);
    this.visit(this.lowPoint.x, this.lowPoint.y + 1, this.map[this.lowPoint.y][this.lowPoint.x]);
  }
  public get size(): number {
    return this.visited.map(r => r.filter(v => v === 1).length).reduce((a,v) => a+v);
  }
}

function findBasins(map: number[][]): Basin[] {
  const basins: Basin[] = []
  for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[y].length; x++) {
      const thisVal = map[y][x];
      const isLowPoint = (x == 0 || map[y][x-1] > thisVal) &&
        (x == (map[y].length -1) || map[y][x+1] > thisVal) &&
        (y == 0 || map[y-1][x] > thisVal) &&
        (y == (map.length -1) || map[y+1][x] > thisVal);
      if (isLowPoint) {
        basins.push(new Basin( { y, x }, map));
      }
    }
  }
  basins.forEach(b => b.grow());
  return basins;
}

const sample_basins = findBasins(sample_input);
// basins.forEach(b => console.log(b.lowPoint, b.size));
const sample_sizes = sample_basins.map(b=> b.size).sort((a,b) => a < b ? 1 : a > b ? -1 : 0).slice(0,3);
// console.log(sizes);
const sample_resp = sample_sizes.reduce((a, v) => a*v, 1);
console.assert(sample_resp === 1134, 'Wrong basin sample answer');


const basins = findBasins(input);
// basins.forEach(b => console.log(b.lowPoint, b.size));
const sizes = basins.map(b=> b.size).sort((a,b) => a < b ? 1 : a > b ? -1 : 0).slice(0,3);
// console.log(sizes);
const resp = sizes.reduce((a, v) => a*v, 1);
console.log('Part 2: What do you get if you multiply together the sizes of the three largest basins?', resp);

