import { countLetters, Histogram, loadInputFile_StrByLine } from "../utils";

const sample_rules: {[key: string]: string} = {
'CH': 'B',
'HH': 'N',
'CB': 'H',
'NH': 'C',
'HB': 'C',
'HC': 'B',
'HN': 'C',
'NN': 'C',
'BH': 'H',
'NC': 'B',
'NB': 'B',
'BN': 'B',
'BB': 'N',
'BC': 'B',
'CC': 'N',
'CN': 'C',
};

const sample_input = 'NNCB';

function step(str: string, rules: {[key: string]: string}): string {
  let resp = str.substring(0,1);
  for (let i = 0; i < (str.length - 1); i++) {
    const pair = str.substring(i, i+2);
    if (rules[pair]) {
      resp = resp + rules[pair] + str.substring(i+1, i+2);
    } else {
      resp = resp + str.substring(i+1, i+2);
    }
  }
  return resp;
}

console.assert(step(sample_input, sample_rules) === 'NCNBCHB', 'Correct expansion');

function multiStep(str: string, rules: {[key: string]: string}, numSteps: number): string {
  if (numSteps === 1) return step(str, rules);
  return step(multiStep(str, rules, numSteps - 1), rules);
}

const sample_after10 = multiStep(sample_input, sample_rules, 10);
console.assert(sample_after10.length === 3073, 'Wrong length');
console.assert([...sample_after10].filter(l => l=== 'B').length === 1749, 'Wrong number of Bs');

const sample_counts = countLetters(sample_after10);
const sample_keys_in_order = sample_counts.keysInOrder();
const sample_delta = sample_counts.counts[sample_keys_in_order[sample_keys_in_order.length-1]] - sample_counts.counts[sample_keys_in_order[0]];
console.assert(sample_delta === 1588, 'Wrong delta');

const input = 'PPFCHPFNCKOKOSBVCFPP';
const input_rules: {[key: string]: string} = {};
loadInputFile_StrByLine(__dirname, 'input_rules.txt').map(l => l.split(' -> ')).forEach(([k, v]) => input_rules[k] = v);

const after10 = multiStep(input, input_rules, 10);
const counts10 = countLetters(after10);
const keys_in_order10 = counts10.keysInOrder();
const delta10 = counts10.counts[keys_in_order10[keys_in_order10.length-1]] - counts10.counts[keys_in_order10[0]];
console.log('Part 1: What do you get if you take the quantity of the most common element and subtract the quantity of the least common element?', delta10);

// const after40 = multiStep(input, input_rules, 40);
// const counts40 = countLetters(after40);
// const keys_in_order40 = counts40.keysInOrder();
// const delta40 = counts40.counts[keys_in_order40[keys_in_order40.length-1]] - counts40.counts[keys_in_order40[0]];
// console.log('Part 2: What do you get if you take the quantity of the most common element and subtract the quantity of the least common element?', delta40);
// Nope ... it doesn't scale

function getPairCounts(str: string): Histogram {
  const h = new Histogram();
  for (let i = 0; i < (str.length - 1); i++) {
    h.increment(str.substring(i, i+2));
  }
  return h;
} 
function stepHistogram(h: Histogram, rules: {[key: string]: string}, numSteps = 1): Histogram {
  const r = new Histogram();
  h.asPairs().map(({key, count}) => {
    const middle = rules[key];
    if (middle) {
      r.increment(key[0] + middle, count);
      r.increment(middle + key[1], count);
    } else {
      r.increment(key, count);
    }
  });
  if (numSteps === 1) return r;
  return stepHistogram(r, rules, numSteps - 1);
}
function splitHistogramLetters(h: Histogram, str: string): Histogram {
  const r = new Histogram();
  h.asPairs().forEach(({key, count}) => {
    r.increment(key[0], count);
    r.increment(key[1], count);
  });
  r.increment(str[0]);
  r.increment(str[str.length-1]);
  r.splitInHalf();
  return r;
}
function getAnswer(h: Histogram): number {
  const kio = h.keysInOrder();
  return h.counts[kio[kio.length-1]] - h.counts[kio[0]];
}

const sample_pairs = getPairCounts(sample_input);
const sample_after_step1 = stepHistogram(sample_pairs, sample_rules, 10);
// console.log(sample_after_step1.asPairs());
const sample_counts_letter = splitHistogramLetters(sample_after_step1, sample_input);
// console.log(sample_counts_letter);
console.assert(sample_counts_letter.counts['B'] === 1749);

const after10_2 = stepHistogram(getPairCounts(input), input_rules, 10);
const counts_2 = splitHistogramLetters(after10_2, input);
const delta10_2 = getAnswer(counts_2);
console.log('Part 1: What do you get if you take the quantity of the most common element and subtract the quantity of the least common element?', delta10_2);


const after40_2 = stepHistogram(getPairCounts(input), input_rules, 40);
const counts_40_2 = splitHistogramLetters(after40_2, input);
console.log(counts_40_2);
const delta40_2 = getAnswer(counts_40_2);
console.log('Part 2: What do you get if you take the quantity of the most common element and subtract the quantity of the least common element?', delta40_2);
