
import { loadInputFile_StrNumByLine } from "../utils";

const input = loadInputFile_StrNumByLine(__dirname, 'input.txt');

class Submarine {
  x = 0
  depth = 0

  move(instruction: {key: string, value: number}) {
    switch (instruction.key) {
      case 'forward':
        this.x += instruction.value;
        break;
      case 'down':
        this.depth += instruction.value;
        break;
      case 'up':
        this.depth -= instruction.value;
        break;
      default:
        throw new Error('unknown instruction: ' + instruction.key);
    }
  }

  getFinalPos(): number {
    return this.x * this.depth
  }
}

const mySub = new Submarine();
input.forEach(i => mySub.move(i));

console.log('Part 1: What do you get if you multiply your final horizontal position by your final depth?', mySub.getFinalPos());


class AimingSubmarine extends Submarine {
  aim = 0;

  move(instruction: {key: string, value: number}) {
    switch (instruction.key) {
      case 'forward':
        this.x += instruction.value;
        this.depth += this.aim * instruction.value;
        break;
      case 'down':
        this.aim += instruction.value;
        break;
      case 'up':
        this.aim -= instruction.value;
        break;
      default:
        throw new Error('unknown instruction: ' + instruction.key);
    }
  }
}

const test = [
  {key: 'forward', value: 5},
  {key: 'down', value: 5},
  {key: 'forward', value: 8},
  {key: 'up', value: 3},
  {key: 'down', value: 8},
  {key: 'forward', value: 2},
];

const myTestSub = new AimingSubmarine()
test.forEach(i => myTestSub.move(i));
console.assert(myTestSub.getFinalPos() === 900, "** Unexpected result!!!");


const myAimingSub = new AimingSubmarine();
input.forEach(i => myAimingSub.move(i));

console.log('Part 2: What do you get if you multiply your final horizontal position by your final depth?', myAimingSub.getFinalPos());
