import { loadInputFile_StrByLine } from "../utils";

class Board {
  size = 5;
  calledAcum = 0;
  numbers: number[][] = [];
  matchesByCol: number[] = [];
  matchesByRow: number[] = [];
  lastCalled: number = 0;
  won = false;

  /**
   * 
   * @param num The number that was called
   * @returns Whether we have bingo or not
   */
  public numberCalled(num: number): boolean {
    for (let row = 0; row < this.size; row++) {
      for (let col = 0; col < this.size; col++) {
        if (this.numbers[row][col] === num) {
          this.calledAcum += num;
          this.lastCalled = num;
          this.matchesByCol[col]++;
          this.matchesByRow[row]++;
          if (this.matchesByCol[col] >= this.size || this.matchesByRow[row] >= this.size) {
            this.won = true;
            return true;
          }
          return false;
        }
      }
    }
    return false;
  }

  public getScore() {
    const allSum = this.numbers.reduce((a, row) => a + row.reduce((b, v) => b + v, 0), 0);
    return this.lastCalled * (allSum - this.calledAcum);
  }

  public toString(): string {
    return this.numbers.map(r => r.join(' ')).join('\n');
  }

  public hasWon(): boolean {
    return this.won;
  }

  static fromStringArray(strs: string[]): Board {
    const resp = new Board();
    resp.numbers = strs.map(row => row.split(/[ ]+/).map(n => parseInt(n, 10)));
    resp.matchesByCol = new Array(5).fill(0);
    resp.matchesByRow = new Array(5).fill(0);
    return resp;
  }
}

class Game {
  lastCalledPos = -1;
  boards: Board[] = []
  callOrder: number[] = []
  winnerBoards = new Set<Board>();

  playNext() {
    this.lastCalledPos++;
    const num = this.callOrder[this.lastCalledPos];
    for (const board of this.boards) {
      if (board.numberCalled(num)) {
        this.winnerBoards.add(board);
      }
    }
  }

  playToLose(): Board {
    while (this.boards.length > 1 && this.lastCalledPos < (this.callOrder.length -1)) {
      this.playNext();
      this.boards = this.boards.filter(b => !b.hasWon());
    }
    if (this.boards.length === 1) {
      const theBoard = this.boards[0];
      while (!theBoard.hasWon()) {
        this.playNext();
      }
      return theBoard;
    }
    throw new Error('More than one loser board');
  }

  play(): Board[] {
    while (this.winnerBoards.size === 0 && this.lastCalledPos < (this.callOrder.length -1)) {
      this.playNext();
    }
    if (this.winnerBoards.size > 0) {
      return Array.from(this.winnerBoards);
    }
    return [];
  }

  static loadFromFile(file: string) {
    const resp = new Game();
    const allContent = loadInputFile_StrByLine(__dirname, file, false);
    const firstLine = allContent.shift() || '';
    resp.callOrder = firstLine.split(',').map(n => parseInt(n, 10));
    allContent.shift();
    while (allContent.length > 0) {
      resp.boards.push(Board.fromStringArray(allContent.splice(0, 5)));
      allContent.shift();
    }
    return resp;
  }
}

const testGame = Game.loadFromFile('example_input.txt');
const testWinnerBoards = testGame.play();
console.log('Test Run Winner Board');
testWinnerBoards.forEach(w => console.log('Score:', w.getScore(), w.toString()));


const firstGame = Game.loadFromFile('input.txt');
const winnerBoards = firstGame.play();
console.log('Part 1: What will your final score be if you choose that board?');
winnerBoards.forEach(w => console.log('Score:', w.getScore(), w.toString()));


const testGame2 = Game.loadFromFile('example_input.txt');
const testLoserBoard = testGame.playToLose();
console.log('Test Run Loser Board');
console.log('Score:', testLoserBoard.getScore(), testLoserBoard.toString());

const secondGame = Game.loadFromFile('input.txt');
const loserBoard = secondGame.playToLose();
console.log('Part 2: Once it wins, what would its final score be?');
console.log('Score:', loserBoard.getScore(), loserBoard.toString());

