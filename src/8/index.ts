import { loadInputFile_StrByLine } from "../utils";

const numbers  = [
                // number ==> num segments
[1,1,1,0,1,1,1], // 0 ==> 6
[0,0,1,0,0,1,0], // 1 ==> 2 **
[1,0,1,1,1,0,1], // 2 ==> 5
[1,0,1,1,0,1,1], // 3 ==> 5
[0,1,1,1,0,1,0], // 4 ==> 4 **
[1,1,0,1,0,1,1], // 5 ==> 5
[1,1,0,1,1,1,1], // 6 ==> 6
[1,0,1,0,0,1,0], // 7 ==> 3 **
[1,1,1,1,1,1,1], // 8 ==> 7 **
[1,1,1,1,0,1,1], // 9 ==> 6
];
const ALL_LETTERS = ['a','b','c','d','e','f','g'];

function andArray(a: number[], b: number[]): number[] {
  return a.map((v, i) => v == 1 ? b[i] == 1 ? 1 : 0 : 0);
}

function orArray(a: number[], b: number[]): number[] {
  return a.map((v, i) => v == 1 ? 1 : b[i] == 1 ? 1 : 0);
}
function notArray(a: number[]): number[] {
  return a.map((v, i) => v == 1 ? 0 : 1);
}
function eqArray(a: number[], b: number[]): boolean {
  return a.map((v, i) => v === b[i]).reduce((a, v) => a && v, true);
}


const LENGHT_5 = orArray(orArray(numbers[2], numbers[3]), numbers[5]);
const LENGHT_6 = orArray(orArray(numbers[0], numbers[6]), numbers[9]);
const LENGTH_4_MINUS_2 = andArray(numbers[4], notArray(numbers[1]));


// [1,0,1,1,1,0,1], // 2 ==> 5
// [1,0,1,1,0,1,1], // 3 ==> 5
// [1,1,0,1,0,1,1], // 5 ==> 5

const LENGTH_5_COUNT_3 = [1,0,0,1,0,0,1];
const LENGTH_5_COUNT_2 = [0,0,1,0,0,1,0];
const LENGTH_5_COUNT_1 = [0,1,0,0,1,0,0];

// [1,1,1,0,1,1,1], // 0 ==> 6
// [1,1,0,1,1,1,1], // 6 ==> 6
// [1,1,1,1,0,1,1], // 9 ==> 6

const LENGTH_6_COUNT_3 = [1,1,0,0,0,1,1];
const LENGTH_6_COUNT_2 = [0,0,1,1,1,0,0];

const sample_entry = 'acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf';

function countUniqSegmentSizeInstances(str: string): number {
  return str.split(' | ')[1].split(' ').filter(s => s.length === 2 || s.length === 3 || s.length === 4 || s.length === 7).length;
}

console.assert(countUniqSegmentSizeInstances(sample_entry) === 0, 'Wrong count');
console.assert(countUniqSegmentSizeInstances('be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe') === 2, 'Wrong count 2');
console.assert(countUniqSegmentSizeInstances('egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb') === 4, 'Wrong count 3');

const lines = loadInputFile_StrByLine(__dirname, 'input.txt');
const totalCount = lines.map(l => countUniqSegmentSizeInstances(l)).reduce((a, n) => a+n, 0);
console.log('Part 1: In the output values, how many times do digits 1, 4, 7, or 8 appear?', totalCount);

class PossibleValues {
  public letters_2 = '';
  public letters_4 = '';
  public letters_5: string[] = [];
  public letters_6: string[] = [];
  public posMapping: {[key: string]: number[]} = {
    a: [ 1, 1, 1, 1, 1, 1, 1],
    b: [ 1, 1, 1, 1, 1, 1, 1],
    c: [ 1, 1, 1, 1, 1, 1, 1],
    d: [ 1, 1, 1, 1, 1, 1, 1],
    e: [ 1, 1, 1, 1, 1, 1, 1],
    f: [ 1, 1, 1, 1, 1, 1, 1],
    g: [ 1, 1, 1, 1, 1, 1, 1]
  };

  public seen(comb: string) {
    if (comb.length == 2) {
      this.letters_2 = comb;
      [...comb].forEach(l => this.posMapping[l] = andArray(this.posMapping[l], numbers[1]))
      
    } else if (comb.length === 3) {
      [...comb].forEach(l => this.posMapping[l] = andArray(this.posMapping[l], numbers[7]))
    } else if (comb.length === 4) {
      this.letters_4 = comb;
      [...comb].forEach(l => this.posMapping[l] = andArray(this.posMapping[l], numbers[4]))

    } else if (comb.length === 5) {
      this.letters_5.push(comb);
      [...comb].forEach(l => this.posMapping[l] = andArray(this.posMapping[l], LENGHT_5))
    } else if (comb.length === 6) {
      this.letters_6.push(comb);
      [...comb].forEach(l => this.posMapping[l] = andArray(this.posMapping[l], LENGHT_6))
    }
  }
  public removeUnambiguous(){
    const unambLetters = ALL_LETTERS.filter(l => this.posMapping[l].reduce((a,n) => a + n , 0) === 1);
    const mask = unambLetters.reduce((mask, l) => andArray(mask, notArray(this.posMapping[l])), [1,1,1,1,1,1,1]);
    ALL_LETTERS.filter(l => !unambLetters.includes(l)).forEach(l => this.posMapping[l] = andArray(this.posMapping[l], mask));
  }
  public reduce() {
    ALL_LETTERS.filter(l => !this.letters_2.includes(l)).forEach(l => this.posMapping[l] = andArray(this.posMapping[l], notArray(numbers[1])));
    ALL_LETTERS.filter(l => !this.letters_4.includes(l)).forEach(l => this.posMapping[l] = andArray(this.posMapping[l], notArray(LENGTH_4_MINUS_2)));
    [...this.letters_4].filter(l => !this.letters_2.includes(l)).forEach(l => this.posMapping[l] = andArray(this.posMapping[l], LENGTH_4_MINUS_2));
    this.removeUnambiguous();
    const counts_length5: {[key: string]: number} = {
      a: 0,
      b: 0,
      c: 0,
      d: 0,
      e: 0,
      f: 0,
      g: 0,
    };
    const counts_length6 = {...counts_length5};
    this.letters_5.forEach(str => [...str].forEach(l => counts_length5[l]++));
    ALL_LETTERS.forEach(l => {
      if (counts_length5[l] === 3) {
        this.posMapping[l] = andArray(this.posMapping[l], LENGTH_5_COUNT_3);
      } else if (counts_length5[l] == 2) {
        this.posMapping[l] = andArray(this.posMapping[l], LENGTH_5_COUNT_2);
      } else if (counts_length5[l] == 1) {
        this.posMapping[l] = andArray(this.posMapping[l], LENGTH_5_COUNT_1);
      }
    });
    this.removeUnambiguous();
    this.letters_6.forEach(str => [...str].forEach(l => counts_length6[l]++));
    ALL_LETTERS.forEach(l => {
      if (counts_length6[l] === 3) {
        this.posMapping[l] = andArray(this.posMapping[l], LENGTH_6_COUNT_3);
      } else if (counts_length6[l] == 2) {
        this.posMapping[l] = andArray(this.posMapping[l], LENGTH_6_COUNT_2);
      }
    });
    this.removeUnambiguous();
  }
  decode(str: string): number {
    const lights = [...str].reduce((acum, l) => orArray(acum, this.posMapping[l]), [0,0,0,0,0,0,0]);
    for (let i = 0; i<=9; i++) {
      if (eqArray(lights, numbers[i])) {
        return i;
      }
    }
    throw new Error('Should have found one!');
  }
}

const test = new PossibleValues();
'acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab'
  .split(' ')
  .sort((a, b) => a.length >= b.length ? -1 : a.length === b.length ? 0 : -1)
  .forEach(s => test.seen(s));
  test.reduce();
// console.log(test.posMapping);

console.assert(test.decode('cdfeb') === 5, 'Wrong deduction');
console.assert(test.decode('fcadb') === 3, 'Wrong deduction');
console.assert(test.decode('cdbaf') === 3, 'Wrong deduction');

function processInputLine(line: string): number {
  const [teach, evaluate] = line.split(' | ');
  const pv = new PossibleValues();
  teach.split(' ')
    .sort((a, b) => a.length >= b.length ? -1 : a.length === b.length ? 0 : -1)
    .forEach(s => pv.seen(s));
  pv.reduce();
  return parseInt(evaluate.split(' ').map(s => pv.decode(s)).join(''), 10);
}

function processInputArray(lines: string[]): number {
  return lines.map(l => processInputLine(l)).reduce((a, v) => a+v, 0);
}

const test_input = loadInputFile_StrByLine(__dirname, 'test_input.txt');
console.assert(processInputArray(test_input) === 61229, 'Wrong test outcome');

const resp2 = processInputArray(lines);
console.log('Part 2: What do you get if you add up all of the output values?', resp2);