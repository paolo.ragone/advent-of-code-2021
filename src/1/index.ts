// https://adventofcode.com/2021/day/1

import { loadInputFile_IntByLine } from "../utils";

const input = loadInputFile_IntByLine(__dirname, 'input.txt');

function checkConsecutivePosition(pos: number, input: number[]): number {
  if (pos == input.length) return 0;
  return ((input[pos] > input[pos-1]) ? 1 : 0) + checkConsecutivePosition(pos + 1, input);
}

console.log('Part 1: How many measurements are larger than the previous measurement?', checkConsecutivePosition(1, input));

function checkSlidingWindowPosition(pos: number, input: number[]): number {
  if (pos == input.length) return 0;
  const curr = input[pos] + input[pos-1] + input[pos-2];
  const prev = input[pos-1] + input[pos-2] + input[pos-3];
  return ((curr > prev) ? 1 : 0) + checkSlidingWindowPosition(pos + 1, input);
}

console.log('Part 2: How many sums are larger than the previous sum?', checkSlidingWindowPosition(3, input));
