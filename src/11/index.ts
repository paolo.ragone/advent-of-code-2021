import { loadInputFile_IntArrNoSpaceByLine } from "../utils";

const sample_input = loadInputFile_IntArrNoSpaceByLine(__dirname, 'sample_input.txt');

function increase(grid: number[][], flashes: number[][], x: number, y: number, delta = 1): boolean {
  if (x < 0 || y < 0) return false;
  if (y >= grid.length || x >= grid[0].length) return false;
  if (flashes[y][x] !== 0) return false;
  grid[y][x] = grid[y][x] + delta;
  if (grid[y][x] > 9 && flashes[y][x] === 0) {
    flashes[y][x] = 1;
    increase(grid, flashes, x - 1, y - 1);
    increase(grid, flashes, x - 1, y);
    increase(grid, flashes, x - 1, y + 1);
    increase(grid, flashes, x, y - 1);
    increase(grid, flashes, x, y + 1);
    increase(grid, flashes, x + 1, y - 1);
    increase(grid, flashes, x + 1, y);
    increase(grid, flashes, x + 1, y + 1);
    grid[y][x] = 0;
    return true;
  }
  return false;
}

function step(grid: number[][]): {grid: number[][], flashes: number[][]} {
  const copy = grid.map(r => r.map(v => v));
  const flashes = grid.map(r => r.map(v => 0));
//  let newFlashes = true;
//  let firstRound = true;
//  while (newFlashes) {
//    newFlashes = false;
    for (let y = 0; y < grid.length; y++) {
      for (let x = 0; x < grid[y].length; x++) {
        increase(copy, flashes, x, y);
        //newFlashes = newFlashes || increase(copy, flashes, x, y);
      }
    }
//  }
  return {grid: copy, flashes};
}

function multiSteps(grid: number[][], numSteps: number):  {grid: number[][], flashes: number[][]} {
  let input_grid = grid;
  let flashesAcum = grid.map(r => r.map(v => 0));
  for (let i = 0; i< numSteps; i++) {
    const resp = step(input_grid);
    input_grid = resp.grid;
    flashesAcum = flashesAcum.map((r, y) => r.map((v, x) => v + resp.flashes[x][y]));
  }
  return {
    grid: input_grid,
    flashes: flashesAcum,
  }
}

const testData = [
  [1, 1, 1, 1, 1],
  [1, 9, 9, 9, 1],
  [1, 9, 1, 9, 1],
  [1, 9, 9, 9, 1],
  [1, 1, 1, 1, 1]
];

const testResp1 = step(testData);
console.assert(testResp1.grid.map(r => r.join('')).join(' ') === '34543 40004 50005 40004 34543', 'Wrong grid after step 1')
console.assert(testResp1.flashes.map(r => r.join('')).join(' ') === '00000 01110 01110 01110 00000', 'Wrong flashes after step 1')

const testResp2 = step(testResp1.grid);
console.assert(testResp2.grid.map(r => r.join('')).join(' ') === '45654 51115 61116 51115 45654', 'Wrong grid after step 2')
console.assert(testResp2.flashes.map(r => r.join('')).join(' ') === '00000 00000 00000 00000 00000', 'Wrong flashes after step 2')

const sample_input_step1 = step(sample_input);
console.assert(sample_input_step1.grid.map(r => r.join('')).join(' ') === '6594254334 3856965822 6375667284 7252447257 7468496589 5278635756 3287952832 7993992245 5957959665 6394862637', 'Wrong grid after sample step 1')

const sample_input_step10 = multiSteps(sample_input, 10);
console.assert(sample_input_step10.grid.map(r => r.join('')).join(' ') === '0481112976 0031112009 0041112504 0081111406 0099111306 0093511233 0442361130 5532252350 0532250600 0032240000', 'Wrong grid after sample step 10')
console.assert(sample_input_step10.flashes.reduce((a, r) => a + r.reduce((a2, v) => a2 + v, 0), 0) === 204, 'Wrong number of flashes after 10 steps');

const sample_input_step100 = multiSteps(sample_input, 100);
console.assert(sample_input_step100.grid.map(r => r.join('')).join(' ') === '0397666866 0749766918 0053976933 0004297822 0004229892 0053222877 0532222966 9322228966 7922286866 6789998766', 'Wrong grid after sample step 100')
console.assert(sample_input_step100.flashes.reduce((a, r) => a + r.reduce((a2, v) => a2 + v, 0), 0) === 1656, 'Wrong number of flashes after 100 steps');

const input = loadInputFile_IntArrNoSpaceByLine(__dirname, 'input.txt');
const input_step100 = multiSteps(input, 100);
const total_flashes_step100 = input_step100.flashes.reduce((a, r) => a + r.reduce((a2, v) => a2 + v, 0), 0);
console.log('Part 1: How many total flashes are there after 100 steps?', total_flashes_step100);

function findStepAllFlash(grid: number[][]):  number {
  let input_grid = grid;
  let i = 0;
  while (i < 1000) {
    i++
    const resp = step(input_grid);
    input_grid = resp.grid;
    const totalFlashes = resp.flashes.reduce((a, r) => a + r.reduce((a2, v) => a2 + v, 0), 0);
    if (totalFlashes === grid.length * grid[0].length) {
      return i;
    }
  }
  throw new Error('Couldnt find it');
}

console.assert(findStepAllFlash(sample_input) === 195, 'Wrong step they all flash');

const step_all_flash = findStepAllFlash(input);
console.log('Part 2: What is the first step during which all octopuses flash?', step_all_flash)