const CONVERT: {[key: string]: string} = {
  '0': '0000',
  '1': '0001',
  '2': '0010',
  '3': '0011',
  '4': '0100',
  '5': '0101',
  '6': '0110',
  '7': '0111',
  '8': '1000',
  '9': '1001',
  'A': '1010',
  'B': '1011',
  'C': '1100',
  'D': '1101',
  'E': '1110',
  'F': '1111',
};

function hex2bin(hex: string): string {
  return [...hex].map(l => CONVERT[l]).join('');
}

function parseLiteral(bin: string): {valueBin: string, read: number} {
  const chunk = bin.substring(0,5);
  if (chunk.startsWith('1')) {
    const rest = parseLiteral(bin.substring(5));
    return {valueBin: chunk.substring(1) + rest.valueBin, read: 5 + rest.read};
  }
  return {valueBin: chunk.substring(1), read: 5};
}

class Packet {
  constructor(public version: number, public type: number, public length: number, public value: number) {}
  getVersionSum(): number {
    return this.version;
  }
}

function calculateValue(type: number, subPackets: Packet[]): number {
  switch(type) {
    case 0:
      return subPackets.reduce((a, p) => a + p.value, 0);
    case 1:
      return subPackets.reduce((a, p) => a * p.value, 1);
    case 2:
      return subPackets.reduce((a, p) => Math.min(a, p.value), Number.MAX_VALUE);
    case 3:
      return subPackets.reduce((a, p) => Math.max(a, p.value), Number.MIN_VALUE);
    case 5:
      return subPackets[0].value > subPackets[1].value ? 1 : 0;
    case 6:
      return subPackets[0].value < subPackets[1].value ? 1 : 0;
    case 7:
      return subPackets[0].value === subPackets[1].value ? 1 : 0;
    default:
      throw new Error('Unknown packet type');
  }
}

class OperatorPacket extends Packet {
  constructor(version: number, type: number, length: number, public subPackets: Packet[]) {
    super(version, type, length, calculateValue(type, subPackets));
  }
  getVersionSum(): number {
    return this.version + this.subPackets.reduce((a, p) => a + p.getVersionSum(), 0);
  }
}

function parseHex(hex: string): Packet {
  return parseBin(hex2bin(hex));
}

function parseBin(bin: string): Packet {
  const version = parseInt(bin.substring(0,3), 2);
  const type = parseInt(bin.substring(3,6), 2);
  // console.log('Version', version);
  // console.log('Type', type);
  if (type === 4) {
    const parsed = parseLiteral(bin.substring(6));
    return new Packet(version, type, parsed.read + 6, parseInt(parsed.valueBin, 2));
  } else {
    const mode = bin.substring(6,7);
    const subPackets: Packet[] = [];
    if (mode === '0') {
      const length = parseInt(bin.substring(7, 7 + 15), 2);
      const read = 22 + length;
      let toParse = bin.substring(22, 22 + length);
      while (toParse.length > 0) {
        const packet = parseBin(toParse);
        subPackets.push(packet);
        toParse = toParse.substring(packet.length);
      }
      return new OperatorPacket(version, type, read, subPackets);
    } else {
      const numSubPackets = parseInt(bin.substring(7, 7 + 11), 2);
      let start = 18;
      for (let i = 0; i < numSubPackets; i++) {
        const packet = parseBin(bin.substring(start));
        subPackets.push(packet);
        start += packet.length;
      }
      return new OperatorPacket(version, type, start, subPackets);
    }
  }
}

console.assert(hex2bin('D2FE28') === '110100101111111000101000', 'Wrong hex2bin');
console.assert(parseLiteral('101111111000101000').valueBin === '011111100101', 'Wrong unpacking of literal')
const test_packet = parseHex('D2FE28');
console.assert(test_packet, 'Wrong parsing of literal');
console.assert(test_packet.value === 2021, 'Wrong parsing of literal')

const sample2 = '38006F45291200';
console.assert(hex2bin(sample2) === '00111000000000000110111101000101001010010001001000000000', 'Wrong unpacking')
const test2 = parseHex(sample2);
console.assert(test2.length === 49);
console.assert((test2 as OperatorPacket).subPackets.length === 2);

const test3 = parseHex('EE00D40C823060');
// console.log(test3);

console.assert(parseHex('8A004A801A8002F478').getVersionSum() === 16)
console.assert(parseHex('620080001611562C8802118E34').getVersionSum() === 12)
console.assert(parseHex('C0015000016115A2E0802F182340').getVersionSum() === 23)
console.assert(parseHex('A0016C880162017C3686B18A3D4780').getVersionSum() === 31)

const input = '2052ED9802D3B9F465E9AE6003E52B8DEE3AF97CA38100957401A88803D05A25C1E00043E1545883B397259385B47E40257CCEDC7401700043E3F42A8AE0008741E8831EC8020099459D40994E996C8F4801CDC3395039CB60E24B583193DD75D299E95ADB3D3004E5FB941A004AE4E69128D240130D80252E6B27991EC8AD90020F22DF2A8F32EA200AC748CAA0064F6EEEA000B948DFBED7FA4660084BCCEAC01000042E37C3E8BA0008446D8751E0C014A0036E69E226C9FFDE2020016A3B454200CBAC01399BEE299337DC52A7E2C2600BF802B274C8848FA02F331D563B3D300566107C0109B4198B5E888200E90021115E31C5120043A31C3E85E400874428D30AA0E3804D32D32EED236459DC6AC86600E4F3B4AAA4C2A10050336373ED536553855301A600B6802B2B994516469EE45467968C016D004E6E9EE7CE656B6D34491D8018E6805E3B01620C053080136CA0060801C6004A801880360300C226007B8018E0073801A801938004E2400E01801E800434FA790097F39E5FB004A5B3CF47F7ED5965B3CF47F7ED59D401694DEB57F7382D3F6A908005ED253B3449CE9E0399649EB19A005E5398E9142396BD1CA56DFB25C8C65A0930056613FC0141006626C5586E200DC26837080C0169D5DC00D5C40188730D616000215192094311007A5E87B26B12FCD5E5087A896402978002111960DC1E0004363942F8880008741A8E10EE4E778FA2F723A2F60089E4F1FE2E4C5B29B0318005982E600AD802F26672368CB1EC044C2E380552229399D93C9D6A813B98D04272D94440093E2CCCFF158B2CCFE8E24017CE002AD2940294A00CD5638726004066362F1B0C0109311F00424CFE4CF4C016C004AE70CA632A33D2513004F003339A86739F5BAD5350CE73EB75A24DD22280055F34A30EA59FE15CC62F9500';
const packets = parseHex(input);
console.log('Part 1: what do you get if you add up the version numbers in all packets?', packets.getVersionSum());

console.assert(parseHex('C200B40A82').value === 3);
console.assert(parseHex('04005AC33890').value === 54);
console.assert(parseHex('880086C3E88112').value === 7);
console.assert(parseHex('CE00C43D881120').value === 9);
console.assert(parseHex('D8005AC2A8F0').value === 1);
console.assert(parseHex('F600BC2D8F').value === 0);
console.assert(parseHex('9C005AC2F8F0').value === 0);
console.assert(parseHex('9C0141080250320F1802104A08').value === 1);
console.log('Part 2: What do you get if you evaluate the expression represented by your hexadecimal-encoded BITS transmission?', packets.value)