import { loadInputFile_StrByLine } from "../utils";

const closingFor: {[key:string]: string} = {
  '(': ')',
  '[': ']',
  '{': '}',
  '<': '>'
};

const corrupt_scores: {[key:string]: number} = {
  ')': 3,
  ']': 57,
  '}': 1197,
  '>': 25137
};

const missing_scores: {[key:string]: number} = {
  ')': 1,
  ']': 2,
  '}': 3,
  '>': 4
};

function validate(str: string, pos: number = 0, stack: string[] = []): {status: string, expected?: string, got?: string, missing?: string[] } {
  if (str === '') return {status: 'OK'};
  if (pos >= str.length) {
    if (stack.length > 0) {
      // incomplete
      return {status:'incomplete', missing: stack};
    }
    return {status: 'OK'};
  }
  const letter = str.substring(pos,pos+1);
  if (closingFor[letter]) { // It's starting a new chunk
    return validate(str, pos+1, [closingFor[letter], ...stack]);
  }
  if (letter == stack[0]) { // Is it closing what we were expecting?
    return validate(str, pos+1, stack.slice(1));
  }
  return {status:'corrupted', expected: stack[0], got: letter };
}

console.assert(validate('([])').status === 'OK', 'Wrong validation 1');
console.assert(validate('[<>({}){}[([])<>]]').status === 'OK', 'Wrong validation 2');

console.assert(validate('(]').status === 'corrupted', 'Wrong validation 3');
console.assert(validate('{()()()>').status === 'corrupted', 'Wrong validation 4');
console.assert(validate('<([]){()}[{}])').status === 'corrupted', 'Wrong validation 5');

const sample_input = loadInputFile_StrByLine(__dirname, 'sample_input.txt');
const sample_score = sample_input.map(l => validate(l)).filter(r => r.status === 'corrupted').map(r => corrupt_scores[r.got || '']).reduce((a,v) => a+ v, 0);

console.assert(sample_score === 26397, 'Wrong score');

const input = loadInputFile_StrByLine(__dirname, 'input.txt');
const score = input.map(l => validate(l)).filter(r => r.status === 'corrupted').map(r => corrupt_scores[r.got || '']).reduce((a,v) => a+ v, 0);
console.log('Part 1: What is the total syntax error score for those errors?', score);

const test1 = validate('[({(<(())[]>[[{[]{<()<>>');
console.assert(test1.status === 'incomplete', 'Wrong status for test');
console.assert(test1.missing?.join('') === '}}]])})]', 'Wrong status for test');

const test2 = validate('{<[[]]>}<{[{[{[]{()[[[]');
console.assert(test2.status === 'incomplete', 'Wrong status for test');
console.assert(test2.missing?.join('') === ']]}}]}]}>', 'Wrong status for test');

function calculateMissingScore(strs: string[]): number {
  return strs.reduce((a,v) => a*5 + missing_scores[v], 0);
}
console.assert(calculateMissingScore([...'])}>']) === 294, 'Wrong missing score')


const sample_missing_scores = sample_input
  .map(l => validate(l))
  .filter(r => r.status === 'incomplete')
  .map(r => calculateMissingScore(r.missing || []))
  .sort((a,b) => a< b ? -1 : a> b ? 1 : 0);
const sample_missing_score = sample_missing_scores[(sample_missing_scores.length - 1)/2];
console.assert(sample_missing_score === 288957, 'Wrong sample');

const input_missing_scores = input
  .map(l => validate(l))
  .filter(r => r.status === 'incomplete')
  .map(r => calculateMissingScore(r.missing || []))
  .sort((a,b) => a< b ? -1 : a> b ? 1 : 0);
const input_missing_score = input_missing_scores[(input_missing_scores.length - 1)/2];
console.log('Part 2: What is the middle score?', input_missing_score);

