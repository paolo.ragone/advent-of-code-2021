import fs from 'fs';
import path from 'path';

export type Point = {x: number, y: number};

export function loadInputFile_IntByLine(dir: string, file: string): number[] {
  const allContent = fs.readFileSync(path.join(dir, file), 'utf-8');
  const lines = allContent.split('\n');
  return lines.filter(l => l.trim().length > 0).map(l => parseInt(l, 10));
}

export function loadInputFile_IntArrNoSpaceByLine(dir: string, file: string): number[][] {
  const allContent = fs.readFileSync(path.join(dir, file), 'utf-8');
  const lines = allContent.split('\n');
  return lines.filter(l => l.trim().length > 0).map(s => [...s].map(l => parseInt(l, 10)));
}

export function loadInputFile_StrByLine(dir: string, file: string, filterEmptyLines = true): string[] {
  const allContent = fs.readFileSync(path.join(dir, file), 'utf-8');
  const lines = allContent.split('\n');
  if (filterEmptyLines)
    return lines.filter(l => l.trim().length > 0).map(l => l.trim());
  return lines.map(l => l.trim());
}

export function loadInputFile_StrNumByLine(dir: string, file: string): {key: string, value:number}[] {
  const allContent = fs.readFileSync(path.join(dir, file), 'utf-8');
  const lines = allContent.split('\n');
  return lines.filter(l => l.trim().length > 0).map(l => l.split(' ')).map(a => ({key: a[0], value: parseInt(a[1], 10)}));
}

export function loadInputFile_PointByLine(dir: string, file: string): Point[] {
  const allContent = fs.readFileSync(path.join(dir, file), 'utf-8');
  const lines = allContent.split('\n');
  return lines.filter(l => l.trim().length > 0).map(l => l.split(',')).map(a => ({x: parseInt(a[0], 10), y: parseInt(a[1], 10)}));
}

export class Histogram {
  public counts: {[key: string]: number} = {};
  increment(val: string, inc = 1) {
    this.counts[val] = (this.counts[val] ? this.counts[val] : 0) + inc;
  }
  keysInOrder(): string[] {
    const keys = Object.keys(this.counts);
    return keys.sort((a, b) => this.counts[a] - this.counts[b]);
  }
  asPairs(): {key: string, count: number}[] {
    const resp: {key: string, count: number}[] = [];
    return Object.keys(this.counts).map(k => ({key: k, count: this.counts[k]}));
  }
  splitInHalf() {
    Object.keys(this.counts).forEach(k => this.counts[k] = this.counts[k] /2);
  }
}
export function countLetters(str: string): Histogram {
  const h = new Histogram();
  [...str].forEach(l => h.increment(l));
  return h;
}