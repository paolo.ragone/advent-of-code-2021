import { loadInputFile_StrByLine } from "../utils";

const sample_inpu = [
  'start-A',
  'start-b',
  'A-c',
  'A-b',
  'b-d',
  'A-end',
  'b-end',
];

class Node {
  public connectedNodes: Node[] = [];
  constructor(public name: string) {}
  public connectTo(node: Node) {
    if (this.connectedNodes.find(n => n.name === node.name) === undefined) {
      this.connectedNodes.push(node);
    }
  }
  getAllPathsFromHere(visited: string[]): string[][] {
    if (this.name === 'end') {
      return [[...visited, this.name]];
    }
    const nextNodes = this.connectedNodes.filter(n => n.name === n.name.toUpperCase() || !visited.includes(n.name));
    const resp: string[][] = [];
    for (const nextNode of nextNodes) {
      resp.push(...nextNode.getAllPathsFromHere([...visited, this.name]));
    }
    return resp;
  }
  getAllPathsFromHereWithOneSmallTwice(visited: string[], alreadyVisitedASmallTwice = false): string[][] {
    if (this.name === 'end') {
      return [[...visited, this.name]];
    }
    const counters = {} as {[key:string]: number};
    visited.filter(n => n !== 'start' && n.toUpperCase() !== n).forEach(n => counters[n] = (counters[n] ? counters[n] : 0) + 1);
    const nextNodes = this.connectedNodes
      .filter(n => n.name !== 'start')
      .filter(n => (n.name === n.name.toUpperCase() || !alreadyVisitedASmallTwice || !visited.includes(n.name)));
    const resp: string[][] = [];
    for (const nextNode of nextNodes) {
      resp.push(...nextNode.getAllPathsFromHereWithOneSmallTwice([...visited, this.name], alreadyVisitedASmallTwice || (nextNode.name.toLowerCase() === nextNode.name && visited.includes(nextNode.name))));
    }
    return resp;
  }
}

class Graph {
  public nodes = new Map<string, Node>();
  getOrCreateNod(name: string): Node {
    const existing = this.nodes.get(name);
    if (existing) return existing;
    const newNode = new Node(name);
    this.nodes.set(name, newNode);
    return newNode;
  }
  connect(a: string, b: string) {
    const nodeA = this.getOrCreateNod(a);
    const nodeB = this.getOrCreateNod(b);
    nodeA.connectTo(nodeB);
    nodeB.connectTo(nodeA);
  }
  addPair(str: string) {
    const parts = str.split('-', 2);
    this.connect(parts[0], parts[1]);
  }
  getAllPaths(): string[][] {
    const startNode = this.getOrCreateNod('start');
    return startNode.getAllPathsFromHere([]);
  }
  getAllPathsWithOneSmallTwice(): string[][] {
    const startNode = this.getOrCreateNod('start');
    return startNode.getAllPathsFromHereWithOneSmallTwice([]);
  }
}

const sampleGraph = new Graph();
sample_inpu.forEach(l => sampleGraph.addPair(l));
const sample_paths = sampleGraph.getAllPaths().map(p => p.join(','));
console.assert(sample_paths.length === 10);
// console.log('Sample Paths:', sample_paths.join('\n'));

const sample3 = [
  'fs-end',
  'he-DX',
  'fs-he',
  'start-DX',
  'pj-DX',
  'end-zg',
  'zg-sl',
  'zg-pj',
  'pj-he',
  'RW-he',
  'fs-DX',
  'pj-RW',
  'zg-RW',
  'start-pj',
  'he-WI',
  'zg-he',
  'pj-fs',
  'start-RW',
];
const sampleGraph3 = new Graph();
sample3.forEach(l => sampleGraph3.addPair(l));
const sample_paths3 = sampleGraph3.getAllPaths().map(p => p.join(','));
console.assert(sample_paths3.length === 226);

const input = loadInputFile_StrByLine(__dirname, 'input.txt');
const inputGraph = new Graph();
input.forEach(l => inputGraph.addPair(l));
const paths = inputGraph.getAllPaths().map(p => p.join(','));
console.log('Part 1: How many paths through this cave system are there that visit small caves at most once?', paths.length);


const sample_paths_withOneSmallTwice = sampleGraph.getAllPathsWithOneSmallTwice().map(p => p.join(','));
// console.log(sample_paths_withOneSmallTwice);
console.assert(sample_paths_withOneSmallTwice.length === 36);
const sample_paths3_with =sampleGraph3.getAllPathsWithOneSmallTwice();
// console.log(sample_paths3_with.length, sample_paths3_with)
console.assert(sample_paths3_with.length === 3509);

const paths_withOneSmallTwice = inputGraph.getAllPathsWithOneSmallTwice().map(p => p.join(','));
console.log('Part 2: how many paths through this cave system are there?', paths_withOneSmallTwice.length)