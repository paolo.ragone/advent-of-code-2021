import { loadInputFile_StrByLine } from "../utils";

const test_input = `0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2`;

class Point {
  constructor(public x: number, public y: number) {}
}

class Line {
  constructor(readonly from: Point, readonly to: Point) {}
  isHorizontalOrVertical(): boolean {
    return this.isHorizontal() || this.isVertical();
  }
  isHorizontal(): boolean {
    return this.from.y === this.to.y;
  }
  isVertical(): boolean {
    return this.from.x === this.to.x;
  }
  isDiagonal(): boolean {
    return Math.abs(this.from.x - this.to.x) === Math.abs(this.from.y - this.to.y);
  }
}

const GRID_SIZE = 100;

class Grid {
  public grid: number[][];
  
  constructor(gridSize: number) {
    this.grid = new Array(gridSize).fill(0).map(_ => new Array(gridSize).fill(0));
  } 
  drawLine(line: Line) {
    if (line.isHorizontal()) {
      const from = Math.min(line.from.x, line.to.x);
      const to = Math.max(line.from.x, line.to.x);
      for (let x = from; x <= to; x++) {
        this.grid[line.from.y][x]++;
      }
    }
    if (line.isVertical()) {
      const from = Math.min(line.from.y, line.to.y);
      const to = Math.max(line.from.y, line.to.y);
      for (let y = from; y <= to; y++) {
        this.grid[y][line.from.x]++;
      }
    }
    if (line.isDiagonal()) {
      const deltaX = line.from.x < line.to.x ? 1 : -1;
      const deltaY = line.from.y < line.to.y ? 1 : -1;
      const steps = Math.abs(line.from.y - line.to.y);
      for (let s = 0; s <= steps; s++) {
        this.grid[line.from.y + s * deltaY][line.from.x + s * deltaX]++;
      }
    }
  }
  countMoreCellsGreaterThan(num: number): number {
    return this.grid.reduce((acum, row) => acum + row.filter(v => v > num).length, 0);
  }
  toString(): string {
    return this.grid.map(row => row.map(n => n===0 ? '.' : n).join(' ')).join('\n');
  }
}

console.assert(new Line(new Point(1,1), new Point(3,3)).isDiagonal(), 'Wrong Diagonal evaluation')
console.assert(new Line(new Point(9,7), new Point(7,9)).isDiagonal(), 'Wrong Diagonal evaluation2')

function parsePoint(str: string): Point {
  const coords = str.split(',', 2).map(n => parseInt(n, 10));
  return new Point(coords[0], coords[1]);
}
console.assert(parsePoint('0,23 ').x === 0 && parsePoint('0,23 ').y === 23, 'Bad parsing of point')

function parseLine(str: string): Line {
  const parts = str.split(' -> ',2);
  return new Line(parsePoint(parts[0]), parsePoint(parts[1]));
}

function parseInput(lines: string[]): Line[] {
  return lines.map(parseLine);
}

const testGrid = new Grid(10);
const testLines = parseInput(test_input.split('\n'));
//console.log(testLines);
testLines.filter(l => l.isHorizontalOrVertical()).forEach(l => testGrid.drawLine(l))
// console.log(testGrid.toString());
// console.log('Number of points with at least 2 overlaps', testGrid.countMoreCellsGreaterThan(1));
console.assert(testGrid.countMoreCellsGreaterThan(1) === 5, 'Wrong count on sample data');


const input = loadInputFile_StrByLine(__dirname, 'input.txt');
const lines = parseInput(input);
const horizontalOrVerticalLines = lines.filter(l => l.isHorizontalOrVertical());
const grid = new Grid(1000);
horizontalOrVerticalLines.forEach(l => grid.drawLine(l));
console.log('Part 1: At how many points do at least two lines overlap?', grid.countMoreCellsGreaterThan(1));

const testGrid2 = new Grid(10);
testLines.filter(l => l.isHorizontalOrVertical() || l.isDiagonal()).forEach(l => testGrid2.drawLine(l))
// console.log(testGrid2.toString());
console.assert(testGrid2.countMoreCellsGreaterThan(1) === 12, 'Wrong count with diagonals');


const grid2 = new Grid(1000);
const hvdLines = lines.filter(l => l.isHorizontal() || l.isVertical() || l.isDiagonal());
hvdLines.forEach(l => grid2.drawLine(l));
console.log('Part 2: At how many points do at least two lines overlap?', grid2.countMoreCellsGreaterThan(1));
