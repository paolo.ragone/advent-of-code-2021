
const SPAWN_DAYS = 7;
const CHILD_DAYS = 2;

const sample_input = "3,4,3,1,2";

const sample = sample_input.split(',').map(n => parseInt(n, 10));

function step(input_arr: number[]): number[] {
  const newOnes = input_arr.filter(n => n === 0).fill(SPAWN_DAYS + CHILD_DAYS - 1);
  const reduced = input_arr.map(n => n > 0 ? n-1 : (SPAWN_DAYS - 1));
  return [ ...reduced, ...newOnes ];
}

function multiStep(input_arr: number[], numSteps: number): number[] {
  if (numSteps == 0) return input_arr;
  return multiStep(step(input_arr), numSteps-1);
}

// let iter = [0];
// for (let i = 1; i <= 40; i++) {
//   iter = step(iter);
//   console.log(i, iter.length);
//   // console.log('After ', i, 'days: ', iter.length, ': ', iter.join(','));
// }

console.assert(multiStep(sample, 18).join(',') === '6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8', 'Wrong outcome');
console.assert(multiStep(sample, 80).length === 5934, 'Wrong number of lanternfish');

const input = '2,1,1,4,4,1,3,4,2,4,2,1,1,4,3,5,1,1,5,1,1,5,4,5,4,1,5,1,3,1,4,2,3,2,1,2,5,5,2,3,1,2,3,3,1,4,3,1,1,1,1,5,2,1,1,1,5,3,3,2,1,4,1,1,1,3,1,1,5,5,1,4,4,4,4,5,1,5,1,1,5,5,2,2,5,4,1,5,4,1,4,1,1,1,1,5,3,2,4,1,1,1,4,4,1,2,1,1,5,2,1,1,1,4,4,4,4,3,3,1,1,5,1,5,2,1,4,1,2,4,4,4,4,2,2,2,4,4,4,2,1,5,5,2,1,1,1,4,4,1,4,2,3,3,3,3,3,5,4,1,5,1,4,5,5,1,1,1,4,1,2,4,4,1,2,3,3,3,3,5,1,4,2,5,5,2,1,1,1,1,3,3,1,1,2,3,2,5,4,2,1,1,2,2,2,1,3,1,5,4,1,1,5,3,3,2,2,3,1,1,1,1,2,4,2,2,5,1,2,4,2,1,1,3,2,5,5,3,1,3,3,1,4,1,1,5,5,1,5,4,1,1,1,1,2,3,3,1,2,3,1,5,1,3,1,1,3,1,1,1,1,1,1,5,1,1,5,5,2,1,1,5,2,4,5,5,1,1,5,1,5,5,1,1,3,3,1,1,3,1';
const fish = input.split(',').map(n => parseInt(n, 10));

const after80 = multiStep(fish, 80);
console.log('Part 1: How many lanternfish would there be after 80 days?: ', after80.length);

//const after256 = multiStep(counters, 256);
//console.log('Part 2: How many lanternfish would there be after 256 days?: ', after256.length);

// const computable = multiStep([8], 256);
// Nope.. it's not computable

function getCounts(nums: number[]): number[] {
  const resp = [0, 0, 0, 0, 0, 0, 0, 0, 0];
  nums.forEach(n => resp[n]++);
  return resp;
}

console.assert(getCounts(sample).join(',') === '0,1,1,2,1,0,0,0,0', 'Wrong counts');

function stepCounts(counts: number[]): number[] {
  return [
    counts[1],
    counts[2],
    counts[3],
    counts[4],
    counts[5],
    counts[6],
    counts[7] + counts[0],
    counts[8],
    counts[0]
  ];
}

function multiStepCount(counts: number[], numSteps: number): number[] {
  if (numSteps == 0) return counts;
  return multiStepCount(stepCounts(counts), numSteps-1);
}


const sample_counters = getCounts(sample);
console.assert(multiStepCount(sample_counters, 18).join(',') === getCounts([6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8]).join(','), 'Wrong outcome');
const after80counts = multiStepCount(sample_counters, 80);
console.assert(after80counts.reduce((a, n) => a+n, 0) === 5934, 'Wrong second implementation');

const input_counters = getCounts(fish);
const after80_2 = multiStepCount(input_counters, 80);
console.log('Part 1: How many lanternfish would there be after 80 days?: ', after80_2.reduce((a, n) => a+n, 0));

const start = new Date().getTime();
const after256_2 = multiStepCount(input_counters, 256);
console.log('Part 2: How many lanternfish would there be after 256 days?: ', after256_2.reduce((a, n) => a+n, 0));
console.log('Took me', (new Date().getTime()) - start, 'ms')