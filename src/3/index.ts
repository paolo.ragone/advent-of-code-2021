import { loadInputFile_StrByLine } from "../utils";

const exampleData = [
  "00100",
  "11110",
  "10110",
  "10111",
  "10101",
  "01111",
  "00111",
  "11100",
  "10000",
  "11001",
  "00010",
  "01010"
];


function bitCountsPerPosition(data: string[]): {'0': number, '1': number}[] {
  const counters: {'0': number, '1': number}[] = []
  for (const datum of data) {
    for (let i = 0; i< datum.length; i++) {
      if (!counters[i]) counters[i] = {'0': 0, '1': 0};
      counters[i][datum[i] == '0' ? '0' : '1']++;
    }
  }
  return counters;
}

function mostCommonBitPerPosition(data: string[]): string {
  return bitCountsPerPosition(data).reduce((acum, count) => acum + (count['0'] > count['1'] ? '0' : '1'), '')
}

function binaryStringToNum(num: string): number {
  return parseInt(num, 2);
}

console.assert(mostCommonBitPerPosition(exampleData) === '10110', 'Wrong Gamma value for sample data')
console.assert(binaryStringToNum('10110') === 22, 'Wrong conversion from binary to num');

function binaryNotAsString(num: string): string {
  return [...num].reduce((acum, char) => acum + (char === '0' ? '1' : '0'), '');
}

console.assert(binaryNotAsString('10110') === '01001', 'Wrong Not value');

const input = loadInputFile_StrByLine(__dirname, 'input.txt');

const gamma = mostCommonBitPerPosition(input);
const epsilon = binaryNotAsString(gamma);
const powerConsumption = binaryStringToNum(gamma) * binaryStringToNum(epsilon);

console.log('Part 1: Gamma', gamma, 'Epsilon', epsilon)
console.log('Part 1: What is the power consumption of the submarine?', powerConsumption);

enum LifeSupport {
  OXYGEN = '1',
  CO2 = '0',
}

function getLifeSupportRating(lookFor: LifeSupport, data: string[]): string {
  let remaining = [...data];
  let pos = 0;
  while (remaining.length > 1 && pos < data[0].length) {
    const counters = bitCountsPerPosition(remaining);
    const countInPos = counters[pos];
    const filterFor = countInPos['0'] === countInPos['1'] ? lookFor as string :
       lookFor === LifeSupport.OXYGEN ? (countInPos['0'] > countInPos['1'] ? '0' : '1') : (countInPos['0'] > countInPos['1'] ? '1' : '0');
    remaining = remaining.filter(d => d.charAt(pos) === filterFor);
    pos++;
  }
  return remaining[0];
}

console.assert(getLifeSupportRating(LifeSupport.OXYGEN, exampleData) === '10111', 'Wrong oxygen generator rating');
console.assert(getLifeSupportRating(LifeSupport.CO2, exampleData) === '01010', 'Wrong CO2 scrubber rating');

const oxygenRating = getLifeSupportRating(LifeSupport.OXYGEN, input);
const co2Rating = getLifeSupportRating(LifeSupport.CO2, input);
console.log('Part 2: Oxygen rating', oxygenRating, 'CO2 scrubber', co2Rating);
console.log('Part 2: What is the life support rating of the submarine?', binaryStringToNum(oxygenRating) * binaryStringToNum(co2Rating));