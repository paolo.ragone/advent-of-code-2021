import { loadInputFile_PointByLine, loadInputFile_StrByLine, Point } from "../utils";

const sample_points: Point[] = [
  { x: 6, y: 10},
  { x: 0, y: 14},
  { x: 9, y: 10},
  { x: 0, y: 3},
  { x: 10, y: 4},
  { x: 4, y: 11},
  { x: 6, y: 0},
  { x: 6, y: 12},
  { x: 4, y: 1},
  { x: 0, y: 13},
  { x: 10, y: 12},
  { x: 3, y: 4},
  { x: 3, y: 0},
  { x: 8, y: 4},
  { x: 1, y: 10},
  { x: 2, y: 14},
  { x: 8, y: 10},
  { x: 9, y: 0},
];

class Paper {
  constructor (public points: Point[]) {
  }
  fold(direction: 'x' | 'y', pos: number) {
    if (direction === "x") {
      this.points.filter(p => p.x > pos).forEach(p => p.x = 2*pos - p.x);
    } else {
      this.points.filter(p => p.y > pos).forEach(p => p.y = 2*pos - p.y);
    }
    this.points.sort((a, b) => a.x < b.x ? -1 : a.x > b.x ? 1 : a.y < b. y ? -1 : a.y > b.y ? 1 : 0);
    const dedup: Point[] = [];
    this.points.forEach(p => {
      if (dedup.find(c => c.x === p.x && c.y === p.y) === undefined) {
        dedup.push(p);
      }
    });
    this.points = dedup;
  }
  toString() {
    const maxX = this.points.map(p => p.x).sort((a, b) => b - a).shift() || 0;
    const maxY = this.points.map(p => p.y).sort((a, b) => b - a).shift() || 0;
    const grid: string[][] = new Array(maxY + 1).fill(0).map(r => new Array(maxX + 1).fill("."));
    this.points.forEach(p => grid[p.y][p.x] = '#');
    return 'Number of points: ' + this.points.length + '\n' +  grid.map(r => r.join('')).join('\n');
  }
}

const samplePaper = new Paper(sample_points);
console.log(samplePaper.toString());
samplePaper.fold("y", 7);
console.log(samplePaper.toString());
samplePaper.fold("x", 5);
console.log(samplePaper.toString());

const input_points = loadInputFile_PointByLine(__dirname, 'input_points.txt');
const paper = new Paper(input_points);

const input_instructions = loadInputFile_StrByLine(__dirname, 'input_instructions.txt');
const dirPos = input_instructions.map(l => l.split(' ')[2]).map(l => l.split('=')).map(([dir,pos]) => ({dir , pos: parseInt(pos, 10)}));

const firstFold = dirPos.shift();
if (!firstFold) throw new Error('No folds');
paper.fold(firstFold.dir  === 'x' ? 'x' : 'y', firstFold.pos);
console.log('Part 1: How many dots are visible after completing just the first fold instruction on your transparent paper?' , paper.points.length);

dirPos.forEach(dp => paper.fold(dp.dir  === 'x' ? 'x' : 'y', dp.pos));
console.log('Part 2: What code do you use to activate the infrared thermal imaging camera system?' , paper.toString());
