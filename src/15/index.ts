import { loadInputFile_IntArrNoSpaceByLine, Point } from "../utils";

const sample_input: number[][] = [
'1163751742',
'1381373672',
'2136511328',
'3694931569',
'7463417111',
'1319128137',
'1359912421',
'3125421639',
'1293138521',
'2311944581',
].map(s => [...s].map(n => parseInt(n, 10)));

class Path {
  public cost: number;
  constructor(readonly grid: number[][], public steps: Point[] = []) {
    this.cost = steps.length === 0 ? Number.MAX_SAFE_INTEGER : this.steps.reduce((a, p) => a + this.grid[p.y][p.x], 0) - this.grid[0][0];
  }
  prepend(x: number, y: number) {
    return new Path(this.grid, [{x,y}, ...this.steps]);
  }
}

function calcMinCostPath(grid: number[][]): Path[][] {
  const minPathGrid: Path[][] = grid.map(r => r.map(v => new Path(grid)));
  const x = grid[0].length-1;
  const y = grid.length-1;
  minPathGrid[y][x] = new Path(grid, [{x, y}]);
  const pointsToVisit: Point[] = [];
  pointsToVisit.push({x, y});
  while (pointsToVisit.length > 0) {
    const point = pointsToVisit.shift();
    if (!point) throw new Error('Shouldn\'t');
    const x = point.x;
    const y = point.y;

    const myPath = minPathGrid[y][x];
    if (x > 0) {
      const newPath = myPath.prepend(x-1, y);
      if (newPath.cost < minPathGrid[y][x-1].cost) {
        // better path!
        minPathGrid[y][x-1] = newPath;
        pointsToVisit.push({x: x-1, y});
      }
    }
    if (x < (grid[0].length - 1)) {
      const newPath = myPath.prepend(x+1, y);
      if (newPath.cost < minPathGrid[y][x+1].cost) {
        // better path!
        minPathGrid[y][x+1] = newPath;
        pointsToVisit.push({x: x+1, y});
      }
    }
    if (y > 0) {
      const newPath = myPath.prepend(x, y-1);
      if (newPath.cost < minPathGrid[y-1][x].cost) {
        // better path!
        minPathGrid[y-1][x] = newPath;
        pointsToVisit.push({x: x, y: y-1});
      }
    }
    if (y < (grid.length - 1)) {
      const newPath = myPath.prepend(x, y+1);
      if (newPath.cost < minPathGrid[y+1][x].cost) {
        // better path!
        minPathGrid[y+1][x] = newPath;
        pointsToVisit.push({x: x, y: y+1});
      }
    }
  }
  return minPathGrid
}

const sample_resp = calcMinCostPath(sample_input);
// console.log('Path costs: ');
// console.log(sample_resp.map(r => r.map(p => p.cost).join('\t')).join('\n'));


const input = loadInputFile_IntArrNoSpaceByLine(__dirname, 'input.txt');
const resp1 = calcMinCostPath(input);
// console.log('Path costs: ');
// console.log(resp1.map(r => r.map(p => p.cost).join('\t')).join('\n'));
console.log('Part 1: What is the lowest total risk of any path from the top left to the bottom right?', resp1[0][0].cost);

function expandGrid5by5(input: number[][]): number[][] {
  const resp = new Array(input.length * 5).fill(0).map(r => new Array(input[0].length * 5).fill(0));
  for (let dy = 0; dy < 5; dy++) {
    for (let dx = 0; dx < 5; dx++) {
      for (let y = 0; y < input.length; y++) {
        for (let x = 0; x < input[0].length; x++) {
          const newVal = input[y][x] + dx + dy;
          resp[dy*input.length+y][dx*input[0].length +x] = newVal > 9 ? newVal - 9 : newVal;
        }
      }
    }
  }
  return resp;
}

const sample_expanded = expandGrid5by5(sample_input);
// console.log(sample_expanded.map(r => r.join('')).join('\n'));
const sample_resp2 = calcMinCostPath(sample_expanded);
console.assert(sample_resp2[0][0].cost === 315, 'Wrong cost on expanded map');

const expanded_input = expandGrid5by5(input);
const resp2 = calcMinCostPath(expanded_input);
console.log('Part 2: What is the lowest total risk of any path from the top left to the bottom right?', resp2[0][0].cost);
