import { loadInputFile_StrByLine } from "../utils";

function add(left: string, right: string): string {
  if (left === '') return right;
  return reduce(`[${left},${right}]`);
}

function addToLastNumber(str: string, n: number): string {
  let pos = str.length - 1;
  let end = -1;
  let start = -1;
  while (pos > 0 && (end === -1 || start === -1)) {
    if (str[pos] >= '0' && str[pos] <= '9') {
      if (end === -1) {
        end = pos;
      }
    } else if (end !== -1) {
      start = pos + 1;
    }
    pos--;
  }
  if (start !== -1 && end !== -1) {
    return str.substring(0, start) + (parseInt(str.substring(start, end + 1), 10) + n) + str.substring(end + 1);
  } else {
    return str;
  }
}


function addToFirstNumber(str: string, n: number): string {
  let pos = 0;
  let end = -1;
  let start = -1;
  while (pos < str.length && (end === -1 || start === -1)) {
    if (str[pos] >= '0' && str[pos] <= '9') {
      if (start === -1) {
        start = pos;
      }
    } else if (start !== -1) {
      end = pos;
    }
    pos++;
  }
  if (start !== -1 && end !== -1) {
    return str.substring(0, start) + (parseInt(str.substring(start, end), 10) + n) + str.substring(end);
  } else {
    return str;
  }
}


function explode(orig: string): string {
  let depth = 0;
  let pos = 0;
  let str = orig;
  while (pos < str.length) {
    const c = str[pos];
    if (c === '[') depth++;
    if (c === ']') depth--;
    if (depth === 5) {
      const left = str.substring(0, pos);
      const p = str.indexOf(']', pos);
      const toExplode = str.substring(pos + 1, p);
      const right = str.substring(p + 1);
      const [l,r] = toExplode.split(',').map(n => parseInt(n, 10));
      str = addToLastNumber(left, l) + '0' + addToFirstNumber(right, r);
      depth--;
    }
    pos++;
  }
  return str;
}

function split(str: string): string {
  let pos = 0;
  let end = -1;
  let start = -1;
  while (pos < str.length && (end === -1 || start === -1)) {
    if (str[pos] >= '0' && str[pos] <= '9') {
      if (start === -1) {
        start = pos;
      }
    } else if (start !== -1) {
      end = pos;
      const theValue = parseInt(str.substring(start, end), 10);
      if (theValue >= 10) {
        const a = Math.floor(theValue/2);
        return `${str.substring(0, start)}[${a},${theValue-a}]${str.substring(end)}`;
      } else {
        start = -1;
        end = -1;
      }
    }
    pos++;
  }
  return str;
}

function reduce(str: string): string {
  const exploded = explode(str);
  if (exploded !== str) {
    return reduce(exploded);
  } else {
    const splitted = split(str);
    if (splitted !== str) {
      return reduce(splitted);
    }
  }
  return str;
}

console.assert(addToLastNumber('[[[[', 1) === '[[[[');
console.assert(addToLastNumber('[7,[6,[5,[4,', 3) === '[7,[6,[5,[7,');

console.assert(addToFirstNumber(']]],1]', 2) === ']]],3]');

console.assert(explode('[[[[[9,8],1],2],3],4]') === '[[[[0,9],2],3],4]');
console.assert(explode('[7,[6,[5,[4,[3,2]]]]]') === '[7,[6,[5,[7,0]]]]');
console.assert(explode('[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]') === '[[3,[2,[8,0]]],[9,[5,[7,0]]]]');

console.assert(split('[[[[0,7],4],[15,[0,13]]],[1,1]]') === '[[[[0,7],4],[[7,8],[0,13]]],[1,1]]');

console.assert(reduce('[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]') === '[[[[0,7],4],[[7,8],[6,0]]],[8,1]]');

const sample_1 = [
  '[1,1]',
  '[2,2]',
  '[3,3]',
  '[4,4]',
  '[5,5]',
  '[6,6]',
];
console.assert(sample_1.reduce((a, v) => add(a, v), '') === '[[[[5,0],[7,4]],[5,5]],[6,6]]');

const sample_2 = [
  '[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]',
  '[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]',
  '[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]',
  '[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]',
  '[7,[5,[[3,8],[1,4]]]]',
  '[[2,[2,2]],[8,[8,1]]]',
  '[2,9]',
  '[1,[[[9,3],9],[[9,0],[0,7]]]]',
  '[[[5,[7,4]],7],1]',
  '[[[[4,2],2],6],[8,7]]',
];
console.assert(sample_2.reduce((a, v) => add(a, v), '') === '[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]');

function magnitude(orig: string): number {
  let str = orig;
  let r = '$';
  while (str !== r) {
    if (r !== '$') str = r;
    r = str.replace(/\[\d+,\d+\]/,(substr) => {
      const [l,r] = substr.substring(1, substr.length-1).split(',').map(n => parseInt(n, 10));
      return `${l* 3 + r * 2}`;
    });
  }
  return parseInt(str, 10);
}

console.assert(magnitude('[1,9]') === 21);
console.assert(magnitude('[[1,2],[[3,4],5]]') === 143);
console.assert(magnitude('[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]') === 3488);

const sample_3 = [
  '[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]',
  '[[[5,[2,8]],4],[5,[[9,9],0]]]',
  '[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]',
  '[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]',
  '[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]',
  '[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]',
  '[[[[5,4],[7,7]],8],[[8,3],8]]',
  '[[9,3],[[9,9],[6,[4,9]]]]',
  '[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]',
  '[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]',
];
console.assert(sample_3.reduce((a, v) => add(a, v), '') === '[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]');
console.assert(magnitude('[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]') === 4140);

const input = loadInputFile_StrByLine(__dirname, 'input.txt');
const resp = input.reduce((a, v) => add(a, v), '');
console.log('Part 1: What is the magnitude of the final sum?', magnitude(resp));

function maxMagnitudeOfTwo(list: string[]): number {
  let maxMagnitude = -1;
  for (let a = 0; a < list.length; a++) {
    for (let b = 0; b < list.length; b++) {
      if (a === b) continue;
      const m = magnitude(add(list[a], list[b]));
      if (m > maxMagnitude) {
        maxMagnitude = m;
      }
    }
  }
  return maxMagnitude;
}

console.assert(maxMagnitudeOfTwo(sample_3) === 3993);

console.log('Part 2: What is the largest magnitude of any sum of two different snailfish numbers from the homework assignment?',
  maxMagnitudeOfTwo(input));